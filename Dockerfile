# This image is supposed to run "once" in a container. Build it and run (once). 
#   docker-compose -f docker-compose.yml build netherlands0
#   docker-compose -f docker-compose.yml run netherlands0
#   docker-compose -f docker-compose.yml run netherlands5
#   docker-compose -f docker-compose.yml run nl1km
# create a file called '.env' with targetUrl to upload images on a (remote) webserver

FROM fedora:25

#Basic environment
ENV BASEDIR=/root/rasp
ENV TZ=Europe/Amsterdam
RUN mkdir $BASEDIR

# required packages
RUN dnf update -y && dnf install -y \
  netcdf-fortran \
  nco \
  libpng15 \
  iproute-tc \
  tcp_wrappers-libs \
  sendmail \
  procmail \
  psmisc \
  procps-ng \
  mailx \
  findutils \
  ImageMagick \
  perl-core \
  perl-CPAN \
  ncl \
  netcdf \
  libpng \
  libjpeg-turbo \
  perl-Test-Harness \
  which \
  patch \
  vim \
  less \
  bzip2 \
  pigz \
  openssh-clients
  
# configure CPAN and install required modules
RUN (echo y;echo o conf prerequisites_policy follow;echo o conf commit) | cpan \
  && cpan install Proc/Background.pm

# fix dependencies
RUN ln -s libnetcdff.so.6 /lib64/libnetcdff.so.5 \
  && ln -s libnetcdf.so.11 /lib64/libnetcdf.so.7

WORKDIR /root/

#
# Download and unpack necessary data
#

# Download and unpack static geographical data directory
# Assuming you already downloaded this file
# See http://www2.mmm.ucar.edu/wrf/users/download/get_sources_wps_geog.html
# ADD https://www.dropbox.com/sh/n25p0nz6bgvjzlb/AABfOgE6bbOVBJjZgYWoHyfma/geog.tar.gz $BASEDIR
COPY geog.tar.gz $BASEDIR
RUN cd $BASEDIR \
  && tar xf geog.tar.gz \
  && rm geog.tar.gz
RUN ls $BASEDIR

COPY geog.fine.tar.gz $BASEDIR
RUN cd $BASEDIR \
  && tar xf geog.fine.tar.gz \
  && rm geog.fine.tar.gz
RUN ls $BASEDIR

# Download and unpack raspGM
# Assuming you already downloaded this file
COPY rasp-gm-stable.tar.gz $BASEDIR
RUN cd $BASEDIR \
  && tar xf rasp-gm-stable.tar.gz --strip-components=1 \
  && rm rasp-gm-stable.tar.gz
RUN ls $BASEDIR

# Download and unpack detailed coastlines and lakes directory
# Assuming you already downloaded this file
# ADD https://www.dropbox.com/sh/n25p0nz6bgvjzlb/AACgTIBZNHLOAW7PxYslVDs2a/rangs.tgz $BASEDIR
COPY rangs.tgz $BASEDIR
RUN cd $BASEDIR \
  && tar xf rangs.tgz \
  && rm rangs.tgz
RUN ls $BASEDIR

#
# Set environment for interactive container shells
#
RUN echo export BASEDIR=$BASEDIR >> /etc/bashrc \
  && echo export PATH+=:\$BASEDIR/bin >> /etc/bashrc

# cleanup 
RUN yum clean all

# Download and unpack NETHERLANDS directory
# Assuming you already downloaded this file
COPY NETHERLANDS.tar.gz $BASEDIR
RUN cd $BASEDIR \
  && tar xf NETHERLANDS.tar.gz \
  && rm NETHERLANDS.tar.gz
RUN ls $BASEDIR
RUN ls $BASEDIR/NETHERLANDS

# Download and unpack NL1KM directory
# Assuming you already downloaded this file
COPY NL1KM-1.tar.gz $BASEDIR
RUN cd $BASEDIR \
  && tar xf NL1KM-1.tar.gz \
  && rm NL1KM-1.tar.gz
RUN ls $BASEDIR
RUN ls $BASEDIR/NL1KM

# Overwrite the existing rasp.run.parameters.NETHERLANDS file (should go to git)
COPY rasp.run.parameters.NETHERLANDS $BASEDIR/NETHERLANDS
COPY rasp.run.parameters.NL1KM $BASEDIR/NL1KM

# Change download links to new format
# Change in ftp2u_subregion.pl "\&dir=\%2Fgfs.$curdate$runTime" into "\&dir=\%2Fgfs.$curdate/$runTime" was already applied
RUN sed -i 's/gfs.%04d%02d%02d%02d/gfs.%04d%02d%02d\/%02d/' $BASEDIR/bin/GM-master.pl

# Prepare NETHERLANDS region
RUN cp -a $BASEDIR/region.TEMPLATE/. $BASEDIR/NETHERLANDS/
RUN ls $BASEDIR
RUN ls $BASEDIR/NETHERLANDS

# Prepare NL1KM region
RUN cp -a $BASEDIR/region.TEMPLATE/. $BASEDIR/NL1KM/
RUN ls $BASEDIR
RUN ls $BASEDIR/NL1KM

# Assuming you already downloaded this file
COPY rasp-gm-NETHERLANDS.tar.gz $BASEDIR
RUN cd $BASEDIR \
  && tar xf rasp-gm-NETHERLANDS.tar.gz --strip-components=1 \
  && rm rasp-gm-NETHERLANDS.tar.gz
RUN ls $BASEDIR
RUN ls $BASEDIR/GM

ENV PATH="${BASEDIR}/bin:${PATH}"

# initialize
RUN cd $BASEDIR/NETHERLANDS/ \
  && wrfsi2wps.pl \
  && wps2input.pl \
  && geogrid.exe

# initialize part 1
RUN cd $BASEDIR/NL1KM/ \
  && wrfsi2wps.pl \
  && wps2input.pl

# Download and unpack NL1KM+ directory
# Assuming you already downloaded this file
COPY NL1KM-2.tar.gz $BASEDIR
RUN cd $BASEDIR \
  && tar xf NL1KM-2.tar.gz \
  && rm NL1KM-2.tar.gz
RUN ls $BASEDIR
RUN ls $BASEDIR/NL1KM
RUN ls $BASEDIR/RUN.TABLES

# initialize part 2
RUN cd $BASEDIR/NL1KM/ \
  && geogrid.exe

RUN rm -rf $BASEDIR/geog

WORKDIR /root/rasp/

VOLUME ["/root/rasp/NETHERLANDS/OUT/", "/root/rasp/NETHERLANDS/LOG/","/root/rasp/NL1KM/OUT/", "/root/rasp/NL1KM/LOG/"]

COPY runRasp.sh meteogram.ncl sitedata.ncl ${BASEDIR}/bin/
COPY logo.png ${BASEDIR}/

# update the url in ftp2u_subregion.pl (we should do this smarter)
COPY url_update_20210323.patch ${BASEDIR}/
RUN patch bin/ftp2u_subregion.pl url_update_20210323.patch

#Run RASP, move the images to the final directory and copy some extra log files
CMD ["runRasp.sh","NETHERLANDS"]


